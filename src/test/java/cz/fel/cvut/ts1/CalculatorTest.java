package cz.fel.cvut.ts1;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import static cz.fel.cvut.ts1.Calculator.*;

public class CalculatorTest {
    @Test
    public void addTest() {
        assertEquals(add(2, 2), 4);
    }

    @Test
    public void subtractTest() {
        assertEquals(subtract(10, 4), 6);
    }

    @Test
    public void multiplyTest() {
        assertEquals(multiply(10, 4), 40);
    }

    @Test
    public void divideTest() {
        assertEquals(divide(30, 5), 6);
    }

    @Test
    public void zeroDivideTest() {
        assertThrows(ArithmeticException.class, () -> divide(10, 0));
    }
}
